function draw() {
    
    var graphData = {};
    graphData["nodes"] = nodes;
    graphData["edges"] = edges;
    
    document.getElementById('sigma_container').innerHTML = " ";
    var s = new sigma({
        graph: graphData,
        renderer: {
            container: document.getElementById('sigma_container'),
            type: 'canvas'
        },
        settings: {
            doubleClickEnabled: false,
            minEdgeSize: 0.5,
            maxEdgeSize: 4,
            enableEdgeHovering: true,
            edgeHoverColor: 'edge',
            defaultEdgeHoverColor: '#000',
            edgeHoverSizeRatio: 1,
            edgeHoverExtremities: true,
        }
    });
}